# SimCraft VRPN

A plugin to enable control of a SimCraft APEX3 motion simulator in Unity.

## Installation
 - Add the SimCraftVRPN package to your Unity project
     - For Unity 2019.3+
         - Open the Packages window (Window->Packages)
         - Click the `+`, then `Add package from git repository`
         - Enter this repository URL: `https://AntonFranzluebbers@bitbucket.org/AntonFranzluebbers/simcraft-vrpn.git`
     - For versions below 2019.3
         - Open the `Packages\manifest.json` text file from your project folder (`Packages` is the same level as `Assets`)
         - Add the following line to the list of packages: `"com.simcraftvrpn": "https://AntonFranzluebbers@bitbucket.org/AntonFranzluebbers/simcraft-vrpn.git",`
 - Run the VRPN server
      - Make sure the server is running on the computer connected to the motion simulator
      - To run the VRPN server, find the `EXE\simcraft_vrpn.exe` file included in the package.
 - Add the script to your project
     - Use `Add Component->SimCraft VRPN->Connection`
     - Modify the `outputs` variable from another script to make the sim move.
        