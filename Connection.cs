﻿using UnityEngine;

namespace SimCraftVRPN {

	/// <summary>
	/// This script needs to be on an object in the scene for communication with the motion simulator to work.
	/// </summary>
	[AddComponentMenu("SimCraft VRPN/Connection")]
	public class Connection : MonoBehaviour {

		[Tooltip("Should be the same as the VRPN server. Default is 3888.")]
		public int serverPort = 3888;

		[Space]

		[Tooltip("Enables or disables receiving data from the sim.")]
		public bool receive = true;
		[Tooltip("Data coming from the sim. The three floats are roll, pitch, and yaw. Don't modify these values, since they are set by VRPN.")]
		public float[] inputs = new float[3];
		[Tooltip("Enables or disables sending data to the sim.")]
		public bool send = true;
		[Tooltip("Data being sent from Unity to the sim. The three floats are roll, pitch, and yaw. Modify these values to move the sim.")]
		public float[] outputs = new float[3];

		void Update() {
			if (receive)
				UnityVRPN.getAnalogInfo("SimCraftVRPN@localhost:" + serverPort, inputs.Length, inputs);
			if (send)
				UnityVRPN.sendAnalogOutput("SimCraftVRPN@localhost:" + serverPort, outputs.Length, outputs);
		}
	}
}
